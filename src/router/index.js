import Vue from 'vue';
import Router from 'vue-router';
import MovieSearch from '@/components/MovieSearch';
import NowPlaying from '@/components/NowPlaying';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'NowPlaying',
      component: NowPlaying,
    },
    {
      path: '/movie-search',
      name: 'MovieSearch',
      component: MovieSearch,
    },
  ],
});
