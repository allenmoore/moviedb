export default {

  formatDate(str) {
    const date = new Date(str);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const fullDate = `${month}/${day}/${year}`;

    return fullDate;
  },

  getData(req, opts, cb) {
    const api = 'https://api.themoviedb.org/3/';
    const apiKey = '9084eae9f770e006ebcba95dbd474e28';
    const type = opts.type;
    const queryVars = opts.queryVars;
    const endpoint = `${api}${type}?api_key=${apiKey}${queryVars}`;

    req.open('get', endpoint);
    req.onload = cb();
    req.send();
  },

  /**
   * Method to return the response results.
   *
   * @author Allen Moore
   * @param  {Object}  res the response Object.
   * @return {Promise}     a promise that resolves with the result of parsing
   *                       the body text as JSON.
   */
  getResponse(res) {
    const results = res.json();

    return results;
  },

  /**
   * Method to return the error message.
   *
   * @author Allen Moore
   * @param  {Object} err the error object.
   * @return {Object}     the error message returned when data fetch fails.
   */
  getError(err) {
    return err;
  },
};
